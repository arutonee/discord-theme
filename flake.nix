{
  description = "Aru\'s Discord Theme";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }@inputs: flake-utils.lib.eachDefaultSystem (
    system: let
      overlays = [ ];
      pkgs = import nixpkgs { inherit system overlays; };
    in {
      devShells.default = pkgs.mkShell rec {
        buildInputs = with pkgs; [
          dart-sass
        ];
        packages = with pkgs; [
          bat
        ];
        shellHook = ''
        '';
      };
    }
  );
}
